<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses',function(Blueprint $table){
            $table->increments('id');
            $table->mediumText('title',500000);
            $table->mediumText('description')->nullable();
            $table->longText('content');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
            $table->string('cover_image')->default('https://s3.us-east-2.amazonaws.com/hugoenglishclub/posts/default_cover.png');
            $table->string('status')->default('public');
            $table->integer('view')->default(0);
            $table->double('rating')->default(4.5);
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
