<?php

use Faker\Generator as Faker;

$factory->define(App\Tag::class, function (Faker $faker) {
    $jsonString = file_get_contents('database/factories/data.json');
    $data = json_decode($jsonString, false);
    $tagName = $data->tag[$faker->unique()->numberBetween($min = 0, $max = count($data->tag)-1)];
    return [
        'name' => $tagName,
        'slug' => str_slug($tagName),
    ];
});
