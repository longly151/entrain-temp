<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(App\User::class, function (Faker $faker) {
    $jsonString = file_get_contents('database/factories/data.json');
    $data = json_decode($jsonString, false);
    $username = strtolower($data->lastName[rand(0,count($data->lastName)-1)].$data->middleName[rand(0,count($data->middleName)-1)].$data->firstName[rand(0,count($data->firstName)-1)].rand(1,500));
    return [
        'fullname' => $data->lastName[rand(0,count($data->lastName)-1)].' '.$data->middleName[rand(0,count($data->middleName)-1)].' '.$data->firstName[rand(0,count($data->firstName)-1)],
        'username' => $username,
        'email' => $username.'@gmail.com',
        'phone_number' => $faker->unique()->regexify('/^0(3[23456789][0-9]{7}|7[0-9]{8}|5[68][0-9]{7}|5[9][0-9]{7}|8[689][0-9]{7}|9[012346789][0-9]{7})/'),
        'address' => rand(1,500).$data->nameRoad[rand(0,count($data->nameRoad)-1)],
        'password' =>  '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'role_id' => 2,
        'status' => $faker->randomElement($array = array ('active','banned','pending')),
    ];
});
