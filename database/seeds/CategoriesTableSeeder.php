<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Cate 1','slug' => 'cate-1','parent_id'=>NULL,'created_at' => now(),'updated_at' => now()],
            ['name' => 'Cate 2','slug' => 'cate-2','parent_id'=>NULL,'created_at' => now(),'updated_at' => now()],
            ['name' => 'Cate 3','slug' => 'cate-3','parent_id'=>NULL,'created_at' => now(),'updated_at' => now()],
            ['name' => 'Cate 4','slug' => 'cate-4','parent_id'=>NULL,'created_at' => now(),'updated_at' => now()],
            ['name' => 'Cate 5','slug' => 'cate-5','parent_id'=>NULL,'created_at' => now(),'updated_at' => now()],
        ]);
    }
}
