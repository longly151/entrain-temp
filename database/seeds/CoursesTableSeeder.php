<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    // S3 seeder
        DB::table('courses')->insert([
            [
                'title' => 'Title 1',
                'description' => 'Description 1',
                'content' => 'Content 1',
                'category_id' => 1,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 1').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Title 2',
                'description' => 'Description 2',
                'content' => 'Content 2',
                'category_id' => 2,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 2').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Title 3',
                'description' => 'Description 3',
                'content' => 'Content 3',
                'category_id' => 3,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 3').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Title 4',
                'description' => 'Description 4',
                'content' => 'Content 4',
                'category_id' => 4,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 4').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Title 5',
                'description' => 'Description 5',
                'content' => 'Content 5',
                'category_id' => 5,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 5').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Title 6',
                'description' => 'Description 6',
                'content' => 'Content 6',
                'category_id' => 1,
                'cover_image' => 'https://s3.us-east-2.amazonaws.com/hugoenglishclub/post/home-20190306142623.jpg',
                'slug' => str_slug('Title 6').'-'.date('Ymdhis'),
                'status' => 'public',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
