<?php

use Illuminate\Database\Seeder;

class CourseTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50; // limit > 20
        $min = 1;
        DB::table('course_tag')->insert([
            [
                'course_id' => 1,
                'tag_id' => 1,
            ],
            [
                'course_id' => 1,
                'tag_id' => 3,
            ],
            [
                'course_id' => 2,
                'tag_id' => 1,
            ],
            [
                'course_id' => 2,
                'tag_id' => 4,
            ],
            [
                'course_id' => 3,
                'tag_id' => 1,
            ],
            [
                'course_id' => 3,
                'tag_id' => 7,
            ],
            [
                'course_id' => 4,
                'tag_id' => 1,
            ],
            [
                'course_id' => 4,
                'tag_id' => 6,
            ],
            [
                'course_id' => 5,
                'tag_id' => 1,
            ],
            [
                'course_id' => 5,
                'tag_id' => 3,
            ],
            [
                'course_id' => 6,
                'tag_id' => 1,
            ],
            [
                'course_id' => 6,
                'tag_id' => 7,
            ],
        ]);
    }
}
