<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;
        
        DB::table('users')->insert([
            [
                'fullname' => 'Enclave Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'phone_number' => '0327575971',
                'address' => '455 Hoang Dieu, Da Nang',
                'avatar' => 'https://s3-us-east-2.amazonaws.com/hugoenglishclub/avatar/default-avatar-20190306141537.png',
                'status' => 'active',
                'password' =>  bcrypt('admin'),
                'role_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'fullname' => 'Enclave Member 1',
                'username' => 'member1',
                'email' => 'member1@gmail.com',
                'avatar' => 'https://s3-us-east-2.amazonaws.com/hugoenglishclub/avatar/default-avatar-20190306141537.png',
                'status' => 'active',
                'phone_number' => '0327575972',
                'address' => '455 Hoang Dieu, Da Nang',
                'password' =>  bcrypt('member1'),
                'role_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'fullname' => 'Enclave Member 2',
                'username' => 'memeber2',
                'email' => 'memeber2@gmail.com',
                'avatar' => 'https://s3-us-east-2.amazonaws.com/hugoenglishclub/avatar/default-avatar-20190306141537.png',
                'status' => 'active',
                'phone_number' => '0327575973',
                'address' => '455 Hoang Dieu, Da Nang',
                'password' =>  bcrypt('memeber2'),
                'role_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
        factory(App\User::class, 10)->create();
    }
}
