<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\softDeletes;

class Course extends Model
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'courses';
    protected $fillable = [
        'title', 'description', 'content',
        'cover_image', 'views', 'rating', 'status', 'slug',
        'created_at', 'updated_at', 'deleted_at'
    ];

    public $timestamp = true;

    public function category() {
        return $this->hasOne('App\Category','id','category_id');
    }
    public function tags() {
        return $this->belongsToMany('App\Tag','course_tag','course_id','tag_id');
    }
    public static function boot() {
        parent::boot();
        self::creating(function ($course) {
            $course['slug'] = str_slug($course['title']).'-'.date('Ymdhis');
        });
        
    }
}
