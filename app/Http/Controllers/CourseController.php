<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;
use App\Tag;
use App\Category;
use App\Course;
use Illuminate\Support\Facades\Redis;
use App\Helpers\Helper;
use Image;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('created_at','desc')->paginate(env('QUANTITY_PER_PAGE'));
        return view('body.course.manage',['courses'=>$courses]);
    }
    public function publish($id)
    {
        $course = Course::find($id);
        $course->status = 'public';
        $course->save();
        return redirect('/course/manage')->with('success','Publish Course successfully ');
    }
    public function unpublish($id)
    {
        $course = Course::find($id);
        $course->status = 'pending';
        $course->save();
        return redirect('/course/manage')->with('success','Unpublish Course successfully ');
    }
    public function bin()
    {
        $courses = Course::onlyTrashed()->orderBy('created_at','desc')->paginate(env('QUANTITY_PER_PAGE'));
        return view('body.course.bin',['courses'=>$courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::select('id','name')->get();
        $topCategories = Category::where('parent_id',NULL)->select('id','name')->get();
        return view('body.course.add',['topCategories'=>$topCategories,'tags'=>$tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $course = $request->except('_token','tags');
        // handle category
        if($course['topCategory'] == '0') {
            unset($course['topCategory']);
            unset($course['category']);
            unset($course['subCategory']);
        } else {
            if ($course['category'] == 0) {
                $course['category'] = $course['topCategory'];
                unset($course['topCategory']);
                unset($course['subCategory']);
            } else {
                if ($course['subCategory'] == 0) {
                    unset($course['topCategory']);
                    unset($course['subCategory']);
                } else {
                    
                    $course['category'] = $course['subCategory'];
                    unset($course['topCategory']);
                    unset($course['subCategory']);
                }
            }
        }
        $dbCourse = new Course;
        $dbCourse->title = $course['title'];
        $dbCourse->description = $course['description'];
        $dbCourse->content = $course['content'];
        $dbCourse->category_id = array_key_exists("category",$course) ? $course['category']:NULL;
        // handle cover image
        if ($request->file('cover_image')) {
            $file = $request->file('cover_image');
            $path = Helper::createS3Url('course', $file);
            $img = Helper::resizeImage('675', $file);
            $dbCourse->cover_image = Helper::s3Upload($path, $img);
        };
        // done
        $dbCourse->status = $request->draft == 'on' ? 'draft':'pending';
        $dbCourse->save();
        $dbCourse->tags()->sync($request->tags);
        return redirect('/course/manage')->with('success','Add course successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::where('id',$id)->first();
        $tags = Tag::select('id','name')->get();
        $topCategories = Category::where('parent_id',NULL)->select('id','name')->get();
        if ($course->category){
            if($course->category->categoryType() == 'topCategory') {
                $course->topCategory = $course->category;
                $course->category = '';
            } else {
                if($course->category->categoryType() == 'category') {
                    $course->topCategory = Category::where('id',$course->category['parent_id'])->first();
                } else {
                    $course->subCategory = $course->category;
                    $course->category = Category::where('id',$course->subCategory['parent_id'])->first();
                    $course->topCategory = Category::where('id',$course->category['parent_id'])->first();
                }
            }
        }
        return view('body.course.edit',['course'=>$course,'topCategories'=>$topCategories,'tags'=>$tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, $id)
    {
        $course = $request->except('_token','tags');

        // handle category
        if($course['topCategory'] == '0') {
            unset($course['topCategory']);
            unset($course['category']);
            unset($course['subCategory']);
        } else {
            if ($course['category'] == 0) {
                $course['category'] = $course['topCategory'];
                unset($course['topCategory']);
                unset($course['subCategory']);
            } else {
                if ($course['subCategory'] == 0) {
                    unset($course['topCategory']);
                    unset($course['subCategory']);
                } else {
                    
                    $course['category'] = $course['subCategory'];
                    unset($course['topCategory']);
                    unset($course['subCategory']);
                }
            }
        }
        
        $dbCourse = Course::find($id);
        $dbCourse->title = $course['title'];
        $dbCourse->description = $course['description'];
        $dbCourse->content = $course['content'];
        $dbCourse->category_id = array_key_exists("category",$course) ? $course['category']:NULL;

        // handle cover image
        if ($request->file('cover_image')) {
            $file = $request->file('cover_image');
            $path = Helper::createS3Url('course', $file);
            $img = Helper::resizeImage('675', $file);
            $dbCourse->cover_image = Helper::s3Upload($path, $img);
        };
        // done
        $dbCourse->status = $request->draft == 'on' ? 'draft':'pending';
        $dbCourse->save();
        $dbCourse->tags()->sync($request->tags);
        return redirect('/course/manage')->with('success','Edit course successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->json()->all();
        Course::where('id',$id)->delete();
        return response()->json([
            'messages' => 'success'
        ],200);
    }
    public function restore(Request $request)
    {
        $id = $request->json()->all();
        Course::where('id',$id)->restore();
        return response()->json([
            'messages' => 'success'
        ],200);
    }
    public function destroy(Request $request)
    {
        $id = $request->json()->all();
        Course::where('id',$id)->forceDelete();
        return response()->json([
            'messages' => 'success'
        ],200);
    }
    
}
