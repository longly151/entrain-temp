<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserProfileRequest;
use App\Http\Requests\UserPasswordRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = session()->get('admin');
        return view('body.profile.profile');
    }
    public function updateInfo(UserProfileRequest $request)
    {
        // get request
        $info = $request->except('_token');
        array_add($info,"remember_token",$request->input('_token'));
        // update database
        $id = session()->get('admin')['id'];
        $user = User::find($id);
        $user->email = $info['email'];
        $user->phone_number = $info['phone_number'];
        $user->address = $info['address'];
        $user->description = $info['description'];
        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $path = Helper::createS3Url('avatar', $file);
            $img = Helper::resizeAvatar('330', $file);
            $user->avatar = Helper::s3Upload($path, $img);
        };
        $user->save();
        $info = User::where('id', $id)->select(['email','phone_number','address','description','avatar'])->first()->toArray();;
        // get cUser
        $cUser = $request->session()->get('admin');
        $cUser['email'] = $info['email'];
        $cUser['phone_number'] = $info['phone_number'];
        $cUser['address'] = $info['address'];
        $cUser['description'] = $info['description'];
        $cUser['avatar'] = $info['avatar'];
        // notification 
        if($cUser==$request->session()->get('admin')) $messages = "Nothing changes";
        else $messages="Change profile successfully";
        // update session
        $request->session()->put('admin',$cUser);

        return redirect('/profile')->with(['success' => $messages,'active'=>'changeInfo']);
    }
    public function updatePassword(UserPasswordRequest $request){
        $password = bcrypt($request->input('password'));
        $id = session()->get('admin')['id'];
        $user = User::find($id)->update(['password' => $password]);
        return redirect('/profile')->with(['success' => 'Change password successfully','active'=>'changePassword']);
    }
}
