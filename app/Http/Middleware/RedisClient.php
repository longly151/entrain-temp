<?php

namespace App\Http\Middleware;

use Closure;
use App\Category;
use App\Course;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;
class RedisClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Category
        $topCategories = Category::where('parent_id',NULL)->select('id','name','slug')->get()->toArray();
        foreach($topCategories as $i => $topCategory){
            $topCategories[$i] = array_add($topCategory,'categories',Category::where('parent_id',$topCategory['id'])->select('id','name','parent_id','slug')->get()->toArray());
            foreach($topCategories[$i]['categories'] as $k => $category){
                $topCategories[$i]['categories'][$k] = array_add($category,'subCategories',Category::where('parent_id',$category['id'])->select('id','name','parent_id','slug')->get()->toArray());
            }
        }
        // if (!Redis::get('topCategories')) {
        //     Redis::set('topCategories',json_encode($topCategories));
        // }
        Redis::set('topCategories',json_encode($topCategories));
        $topCategories = json_decode(Redis::get('topCategories'),true);
        // Menu Course
        $menuCourses = Course::whereRaw('category_id < 5')->select('id', 'title', 'cover_image', 'category_id', 'slug')->get();
        
        View::share(['topCategories'=> $topCategories,'menuCourses'=>$menuCourses]);
        return $next($request);
    }
}
