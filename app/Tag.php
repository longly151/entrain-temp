<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'slug'
    ];
    protected $table = 'tags';
    public $timestamp = true;
    
    public function posts() {
        belongsToMany('App\Post','post_tag','tag_id','post_id');
    }
    public static function boot() {
        parent::boot();
        self::saving(function ($tag) {
            $tag['slug'] = str_slug($tag['name']);
        });
    }
}
