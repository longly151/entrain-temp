<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'username', 'email', 'phone_number',
        'description', 'address', 'password',
        'position', 'avatar', 'status', 'role_id',
        'created_at', 'updated_at', 'deleted_at'
    ];
    protected $table = 'users';
    public $timestamp = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function role() {
        return $this->hasOne('App\Role','id','role_id');
    }
    public static function boot() {
        parent::boot();
        self::creating(function ($user) {
            if(strpos($user['phone_number'],'+84') === 0) {
                $user['phone_number'] = str_replace('+84','0',$user['phone_number']);
            } elseif (strpos($user['phone_number'],'84') === 0) {
                $user['phone_number'] = str_replace('84','0',$user['phone_number']);
            }
            $user['password'] = bcrypt($user['password']);
        });
        self::updating(function ($user) {
            if(strpos($user['phone_number'],'+84') === 0) {
                $user['phone_number'] = str_replace('+84','0',$user['phone_number']);
            } elseif (strpos($user['phone_number'],'84') === 0) {
                $user['phone_number'] = str_replace('84','0',$user['phone_number']);
            }
        });
        
    }


}
