const alertDeleteTag = (tag) => {
    const id = $(tag).data('id');
    swal({
            title: 'Delete tag ?',
            text: 'The tag will be removed from posts. This action does not delete related posts',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/tag/delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the tag, please try again',
                            });
                        }
                    });
            }
        });
};
const alertDeleteCategory = (category) => {
    const id = $(category).data('id');
    swal({
            title: 'Delete Category ?',
            text: 'Subcategories related will be erased. The category will be removed from posts. This action does not delete related posts',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/category/delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the category, please try again',
                            });
                        }
                    });
            }
        });
};
const alertDeleteCourse = (course) => {
    const id = $(course).data('id');
    swal({
            title: 'Delete Course ?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/course/delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the course, please try again',
                            });
                        }
                    });
            }
        });
};
const alertDeleteUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Delete User ?',
            text: 'The user\'s posts, categories and tags will be deleted and cannot be restored',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/user/delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the user, please try again',
                            });
                        }
                    });
            }
        });
};
const alertBanUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Ban User ?',
            text: 'After being banned, the user cannot access the system. Posts, categories and tags remain the same',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/user/ban', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Banned',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to ban the user, please try again',
                            });
                        }
                    });
            }
        });
};
const alertDeleteCarousel = (carousel) => {
    const id = $(carousel).data('id');
    swal({
            title: 'Delete carousel ?',
            text: 'The carousel will be removed from home page',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/carousel/delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the carousel, please try again',
                            });
                        }
                    });
            }
        });
};


const alertRestoreTag = (tag) => {
    const id = $(tag).data('id');
    swal({
            title: 'Restore tag ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/tag/restore', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to restore the tag, please try again',
                            });
                        }
                    });
            }
        });
};
const alertRestoreCategory = (category) => {
    const id = $(category).data('id');
    swal({
            title: 'Restore Category ?',
            text: 'Parent Categories related will be restored',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/category/restore', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to restore the category, please try again',
                            });
                        }
                    });
            }
        });
};
const alertRestoreCourse = (course) => {
    const id = $(course).data('id');
    swal({
            title: 'Restore Course ?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/course/restore', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to restore the course, please try again',
                            });
                        }
                    });
            }
        });
};
const alertRestoreUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Restore User ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/user/restore', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to restore the user, please try again',
                            });
                        }
                    });
            }
        });
};
const alertActiveUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Active User ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/user/active', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to active the user, please try again',
                            });
                        }
                    });
            }
        });
};
const alertRestoreCarousel = (carousel) => {
    const id = $(carousel).data('id');
    swal({
            title: 'Restore carousel ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/carousel/restore', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Success',
                                '',
                                'success',
                            )
                        } else {
                            location.reload();
                            swal({
                                title: 'Error',
                                text: 'Fail to restore the carousel, please try again',
                            });
                        }
                    });
            }
        });
};



const alertCompletedDeleteTag = (tag) => {
    const id = $(tag).data('id');
    swal({
            title: 'Delete tag completely ?',
            text: 'The tag cannot be restored. The tag will be completely removed from posts. This action does not delete related posts',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/tag/completed-delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the tag, please try again',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteCategory = (category) => {
    const id = $(category).data('id');
    swal({
            title: 'Delete category completely ?',
            text: 'The category and subcategories related will be deleted and cannot be restored. The related posts will also be completely deleted ',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/category/completed-delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the category, please try again',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteCourse = (course) => {
    const id = $(course).data('id');
    swal({
            title: 'Delete course completely ?',
            text: 'The course cannot be restored',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/course/completed-delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the course, please try again',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteUser = (user) => {
    const id = $(user).data('id');
    swal({
            title: 'Delete user completely ?',
            text: 'The user cannot be restored',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/user/completed-delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Failed to delete the user, please try again',
                            });
                        }
                    });
            }
        });
};
const alertCompletedDeleteCarousel = (carousel) => {
    const id = $(carousel).data('id');
    swal({
            title: 'Delete carousel completely ?',
            text: 'The carousel cannot be restored',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
        })
        .then((result) => {
            if (result.value) {
                fetch(location.protocol + '//' + location.hostname + '/carousel/completed-delete', {
                        method: 'POST',
                        body: JSON.stringify({
                            id,
                        }),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    })
                    .then((value) => {
                        if (value.status === 200) {
                            location.reload();
                            Swal(
                                'Deleted',
                                '',
                                'success',
                            )
                        } else {
                            swal({
                                title: 'Error',
                                text: 'Fail to delete the carousel, please try again',
                            });
                        }
                    });
            }
        });
};

const postImageMain = (file) => {
    $('#imageMain').attr('hidden', true);
    $('#loadImage').removeAttr('hidden');
    const fileData = $(file).prop('files')[0];
    const formData = new FormData();
    formData.append('file', fileData);
    $.ajax({
        url: '/course/image',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
    }).done((result) => {
        $('#imageMain').val(result.location);
        $('#imageMain').removeAttr('hidden');
        $('#loadImage').attr('hidden', true);
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${result.location}" alt="">`);
    });
};

$(document).ready(() => {
    $('.deleteTag').click(function handle() {
        const tag = this;
        alertDeleteTag(tag);
    });
    $('.deleteCategory').click(function handle() {
        const category = this;
        alertDeleteCategory(category);
    });
    $('.deleteCourse').click(function handle() {
        const course = this;
        alertDeleteCourse(course);
    });
    $('.deleteUser').click(function handle() {
        const user = this;
        alertDeleteUser(user);
    });
    $('.banUser').click(function handle() {
        const user = this;
        alertBanUser(user);
    });
    $('.deleteCarousel').click(function handle() {
        const carousel = this;
        alertDeleteCarousel(carousel);
    });


    $('.restoreTag').click(function handle() {
        const tag = this;
        alertRestoreTag(tag);
    });
    $('.restoreCategory').click(function handle() {
        const category = this;
        alertRestoreCategory(category);
    });
    $('.restoreCourse').click(function handle() {
        const course = this;
        alertRestoreCourse(course);
    });
    $('.restoreUser').click(function handle() {
        const user = this;
        alertRestoreUser(user);
    });
    $('.activeUser').click(function handle() {
        const user = this;
        alertActiveUser(user);
    });
    $('.restoreCarousel').click(function handle() {
        const carousel = this;
        alertRestoreCarousel(carousel);
    });


    $('.completedDeleteTag').click(function handle() {
        const tag = this;
        alertCompletedDeleteTag(tag);
    });
    $('.completedDeleteCategory').click(function handle() {
        const category = this;
        alertCompletedDeleteCategory(category);
    });
    $('.completedDeleteCourse').click(function handle() {
        const course = this;
        alertCompletedDeleteCourse(course);
    });
    $('.completedDeleteUser').click(function handle() {
        const user = this;
        alertCompletedDeleteUser(user);
    });
    $('.completedDeleteCarousel').click(function handle() {
        const carousel = this;
        alertCompletedDeleteCarousel(carousel);
    });

    // file
    $('#file').change(function handle() {
        const file = this;
        postImageMain(file);
    });
    $('#closeImg').click(function handle() {
        $('#imageMain').val('');
        $('#closeImg').attr('hidden', true);
    });
    if ($('#imageMain').val()) {
        const link = $('#imageMain').val();
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${link}" alt="">`);
    }
    $('#imageMain').keyup(function handle() {
        const link = $('#imageMain').val();
        $("img[id='demo']").remove();
        $('#closeImg').removeAttr('hidden');
        $('#demoImg').append(`<img id="demo" src="${link}" alt="">`);
    })

});
