<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login','LoginController@getLogin');
    Route::post('login','LoginController@postLogin');

    Route::get('register','RegisterController@getRegister');
    Route::post('register','RegisterController@postRegister');

    Route::get('logout','LoginController@logout');
});

Route::group(['middleware' => 'adminLogin'], function () {
    Route::get('/', function () {
        return view('body.home');
    });
    Route::group(['middleware' => 'checkRole:1'], function () {
        Route::group(['prefix' => 'course'], function() {
            Route::get('add', 'CourseController@create');
            Route::post('add', 'CourseController@store');
            Route::get('manage', 'CourseController@index');
            Route::get('bin', 'CourseController@bin');
            Route::get('publish/{id}', 'CourseController@publish');
            Route::get('unpublish/{id}', 'CourseController@unpublish');
            Route::get('edit/{id}', 'CourseController@edit');
            Route::post('edit/{id}', 'CourseController@update');
            Route::post('delete', 'CourseController@delete');
            Route::post('restore', 'CourseController@restore');
            Route::post('completed-delete', 'CourseController@destroy');
        });
        Route::group(['prefix' => 'category'], function() {
            Route::get('add', 'CategoryController@create');
            Route::post('add', 'CategoryController@store');
            Route::get('manage', 'CategoryController@index');
            Route::get('bin', 'CategoryController@bin');
            Route::group(['middleware' => 'authorRole:category'], function () {
                Route::get('edit/{id}', 'CategoryController@edit');
                Route::post('edit/{id}', 'CategoryController@update');
                Route::post('delete', 'CategoryController@delete');
                Route::post('restore', 'CategoryController@restore');
                Route::post('completed-delete', 'CategoryController@destroy')->middleWare('checkRole:1');
            });
        });
        Route::group(['prefix' => 'tag'], function() {
            Route::get('add', 'TagController@create');
            Route::post('add', 'TagController@store');
            Route::get('manage', 'TagController@index');
            Route::get('bin', 'TagController@bin');
            Route::group(['middleware' => 'authorRole:tag'], function () {
                Route::get('edit/{id}', 'TagController@edit');
                Route::post('edit/{id}', 'TagController@update');
                Route::post('delete', 'TagController@delete');
                Route::post('restore', 'TagController@restore');
                Route::post('completed-delete', 'TagController@destroy')->middleWare('checkRole:1');
            });
        });
    });
    Route::group(['prefix' => 'user'], function() {
        Route::get('/list', 'UserController@list');
        Route::get('/view/{id}','UserController@show');
        Route::group(['middleware' => 'checkRole:1'], function () {
            Route::get('manage','UserController@index');
            Route::post('manage/{id}','UserController@update');
            Route::post('ban', 'UserController@ban');
            Route::post('delete', 'UserController@delete');
    
            Route::get('inactive', 'UserController@inactive');
            Route::post('active', 'UserController@active');
    
            Route::get('bin', 'UserController@bin');
            Route::post('restore', 'UserController@restore');
            Route::post('completed-delete', 'UserController@destroy')->middleWare('checkRole:1');
        });
    });
    Route::group(['prefix' => 'profile'], function() {
        Route::get('/', 'ProfileController@index');
        Route::get('change-info', function () {
            return redirect('profile')->with(['active'=>'changeInfo']);
        });
        Route::post('change-info', 'ProfileController@updateInfo');
        Route::get('change-password', function () {
            return redirect('profile')->with(['active'=>'changePassword']);
        });
        Route::post('change-password', 'ProfileController@updatePassword');
    });
    Route::group(['prefix' => 'ajax'], function() {
        Route::get('category/{id}', 'AjaxController@getCategories');
    });
});

Route::group(['prefix' => 'error'], function() {
    Route::get('400', function () {
        return view('body.error.400');
    });
    Route::get('403', function () {
        return view('body.error.403');
    });
    Route::get('404', function () {
        return view('body.error.404');
    });
    Route::get('500', function () {
        return view('body.error.500');
    });
    Route::get('503', function () {
        return view('body.error.503');
    });
});